import pytest
from max_dist import max_dist, InvalidArgument


def test_A1():
    with pytest.raises(InvalidArgument):
        assert max_dist() is None


def test_A2():
    with pytest.raises(InvalidArgument):
        assert max_dist([]) is None


def test_A3():
    with pytest.raises(InvalidArgument):
        assert max_dist([1]) is None


def test_C():
    with pytest.raises(TypeError):
        assert max_dist(["a", 2, 3]) is None


def test_D1():
    assert max_dist([1, 2, 3]) == 2


def test_D2():
    assert max_dist([1, 2, -3]) == 2


