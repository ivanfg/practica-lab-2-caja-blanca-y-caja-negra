from dos_menores import dos_menores


def test_A1():
    assert dos_menores() is None


def test_A2():
    assert dos_menores([]) is None


def test_B():
    assert dos_menores([1]) == 1


def test_C1():
    assert dos_menores([1, 2]) == (1, 2)


def test_C2():
    assert dos_menores([2, 1]) == (1, 2)


def test_D():
    assert dos_menores([1, 2, 3]) == (1, 2)


def test_E1():
    assert dos_menores([3, 2, 1]) == (1, 2)


def test_E2():
    assert dos_menores([1, 2, 3]) == (1, 2)


def test_E3():
    assert dos_menores([3, 1, 2]) == (1, 2)


