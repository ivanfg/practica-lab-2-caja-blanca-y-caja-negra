from day_of_year import day_of_year, InvalidArgument
import pytest


def test_A():
    with pytest.raises(InvalidArgument):
        assert day_of_year() is None


def test_B():
    with pytest.raises(InvalidArgument):
        assert day_of_year("a", 2, 2022) is None


def test_C():
    with pytest.raises(InvalidArgument):
        assert day_of_year(-1, 2, 2022) is None


def test_D1():
    with pytest.raises(InvalidArgument):
        assert day_of_year(31, 6, 2022) is None


def test_D2():
    assert day_of_year(29, 2, 2000) == 60


def test_E():
    assert day_of_year(27, 2, 2022) == 58








