class InvalidArgument(Exception):
    print("Function called with invalid arguments")


def max_dist(a=None):
    if type(a) != list or len(a) < 2:  # 1
        raise InvalidArgument

    for i in a:  # 2
        if not type(i) is int:  # 3
            raise TypeError

    lista_valor_absoluto = [abs(i) for i in a]  # 4
    maximo = max(lista_valor_absoluto)
    minimo = min(lista_valor_absoluto)
    resultado = maximo - minimo
    return resultado
