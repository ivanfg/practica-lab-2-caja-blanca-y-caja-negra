def dos_menores(a=None):
    if type(a) != list or len(a) < 1:  # 1
        return None

    min1 = 0
    min2 = 0

    min1 = a[0]

    if len(a) == 1:  # 2
        return min1
    else:  # 3
        e = a[1]
        if e < min1:  # 4
            min2 = min1
            min1 = e
        #######Corrección de código#######
        else:
            min2 = e
        #######Corrección de código#######
        for i in range(2, len(a)):  # 5
            e = a[i]
            if e < min1:  # 6
                min2 = min1
                min1 = e
            elif e < min2:  # 7
                min2 = e
    return min1, min2
