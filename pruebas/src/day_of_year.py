import datetime


class InvalidArgument(Exception):
    print("Function called with invalid arguments")


def day_of_year(day=None, month=None, year=None):
    if day is None or month is None or year is None:  # 1
        raise InvalidArgument

    if not type(day) is int or not type(month) is int or not type(year) is int:  # 2
        raise InvalidArgument

    if day > 31 or day < 1 or month > 12 or month < 1 or year > 2022 or year < 0:  # 3
        raise InvalidArgument

    if (day > 28 and month == 2 and year % 4 != 0) or (day > 29 and month == 2 and year % 4 == 0) \
            or (day > 30 and month == 4) or (day > 30 and month == 6) \
            or (day > 30 and month == 9) or (day > 30 and month == 11):  # 4
        raise InvalidArgument

    date = datetime.date(year, month, day)
    num_dia = date.strftime("%j")
    print(num_dia)
    return int(num_dia[1:])
